# Vietnamese Documents OCR 

My code for project Vietnamese Documents OCR

![Documents OCR](https://icon-library.com/images/ocr-icon/ocr-icon-7.jpg)

The main goal of this project is to build an OCR system to automatically recognize text in Vietnamese documents.

### Dataset

- Dataset used in this project is [Mobile captured receipts OCR (MC-OCR)](https://www.rivf2021-mc-ocr.vietnlp.com/). [MC-OCR](https://www.rivf2021-mc-ocr.vietnlp.com/) dataset used for task recognizing text (Vietnamese and English) from structured and semi-structured receipts in general captured by mobile devices. MC-OCR is a very challenging dataset because of the complexity of mobile captured images such as receipts might be crumpled or the content might be blurred and different from scanned images the quality of photos taken with mobile devices is very diverse because of the light condition and the dynamic environment (e.g., in-door, out-door, complex background, etc.) where the receipts were captured. So, OCR models may have low recognition quality on this dataset.
- For easy download, I uploaded this dataset on [kaggle](https://www.kaggle.com/datasets/hungkhoi/vietnamese-receipts-mcocr-dataset). Some examples of receipts captured by mobile in MC-OCR dataset:

![Example Image 1](dataset/examples/input/mcocr_public_145013bldqx.jpg)

![Example Image 2](dataset/examples/input/mcocr_public_145013clltn.jpg)

![Example Image 3](dataset/examples/input/mcocr_public_145013giwqv.jpg)

![Example Image 4](dataset/examples/input/mcocr_public_145014iwhec.jpg)

![Example Image 5](dataset/examples/input/mcocr_public_145014ckynq.jpg)

- Structure of dataset folder should be:
```
dataset
├── mcocr_public_train_test_shared_data
│   ├── mcocr_train_data
│   │   ├──train_images
│   │   ├──mcocr_train_df.csv
│   ├── mcocr_val_data
│   │   ├──val_images
│   │   ├──mcocr_val_sample_df.csv
│   │   ├──results.csv
├── preprocessor
├── text_detector
├── rotation_corrector
├── text_recognizer
├── key_information_extractor
```

### Installation

- Install packages: 

```
python3 -m venv ./vietnamese_documents_ocr_env
source vietnamese_documents_ocr_env/bin/activate
pip3 install -r requirements.txt
```

- Install gpu version of paddlepaddle (If use cpu, please skip this step):
    - Check version of CUDA:

        ```
        nvcc --version
        ```

    - Install paddlepaddle-gpu:
        - Version 10.2:

        ```
        python -m pip install paddlepaddle-gpu==2.2.2 -i https://mirror.baidu.com/pypi/simple
        ```
        
        - Other version:
        ```
        python -m pip install paddlepaddle-gpu==2.2.2.post{cuda_version} -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html
        ```
    
        - Example for install paddlepaddle-gpu for CUDA 11.0

        ```
        python -m pip install paddlepaddle-gpu==2.2.2.post110 -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html
        ```

### Pipeline

- **Stage 1 - Preprocessing**: Preprocessing is an important step when dealing with Document OCR tasks. Because the documents are not in a certain position in the image and it isn't always straight, even and legible, there are documents that are tilted. In addition, removing the background is necessary otherwise the text recognition models will also recognize texts in the background. To improve the accuracy of text detection and recognition models, applying document scanning will make documents straighter, easier to read and help remove unnecessary background.
- **Stage 2 - Text Detection**: Text detection is an important task of OCR. To recognize the texts in the documents, the first step is to find text present in a image. And in the Document OCR tasks, most text detection models treat text detection as an object detection problem with the main object being a certain word or a segment of many words. 
- **Stage 3 - Rotation Correction**: Because the documents can be tilted, so the documents need to be rotated straight. From the coordinates of the bounding boxes surrounding the text, we can determine the horizontal angle of the document by calculating arctan of the ratio of y coordinate to x coordinate of the longer edge vector of the bounding box to determine the inclination of the text and rotate the document straight back. But after the document is rotated straight, the document can still be rotated 180 degree upside down making the text recognition model's result recognition very bad because the text is upside down. Therefore, we need to rotate straight back for documents that are rotated 180 degree upside down.
- **Stage 4 - Text Recognition**: Text recognition is one of two primary components of the OCR task. This is the task of transcribing the text present in an image. Texts in an image will be cropped according to the coordinates of the bounding boxes and then it will be recognized by the text recognition model.
- **Stage 5 - Key Information Extraction**: Key Information Extraction (KIE) can be treated as the downstream task of OCR. The aim of KIE is to extract texts of a number of key fields from documents. In the MC-OCR dataset, the information fields that need to be extracted are SELLER, ADDRESS, TIMESTAMP, TOTAL_COST. 

### Preprocessing

- Folder ./src/preprocessor contains code for preprocessing the documents. The preprocessing step will scan the document using the Canny algorithm for edge detection and then find the contour with the largest area and then apply the perspective transform based on four points (top-left, top-right, bottom-left, bottom-right) of the largest contour.

- Example Image:

| Input Image       | Edge Image  | Output Image|
| :---------------:| :---------------:| :---------------:|
|![example_01](./dataset/examples/input/mcocr_public_145013acjke.jpg) | ![edge_example_01](./dataset/examples/preprocessor/edges/mcocr_public_145013acjke_canny.jpg) | ![output_example_01](./dataset/examples/preprocessor/imgs/mcocr_public_145013acjke.jpg) |
|![example_02](./dataset/examples/input/mcocr_public_145013pedoz.jpg) | ![edge_example_02](./dataset/examples/preprocessor/edges/mcocr_public_145013pedoz_canny.jpg) | ![output_example_02](./dataset/examples/preprocessor/imgs/mcocr_public_145013pedoz.jpg) |
|![example_03](./dataset/examples/input/mcocr_public_145014ehtue.jpg) | ![edge_example_03](./dataset/examples/preprocessor/edges/mcocr_public_145014ehtue_canny.jpg) | ![output_example_03](./dataset/examples/preprocessor/imgs/mcocr_public_145014ehtue.jpg) |
|![example_04](./dataset/examples/input/mcocr_val_145114rqxas.jpg) | ![edge_example_04](./dataset/examples/preprocessor/edges/mcocr_val_145114rqxas_canny.jpg) | ![output_example_04](./dataset/examples/preprocessor/imgs/mcocr_val_145114rqxas.jpg) |
|![example_05](./dataset/examples/input/mcocr_public_145013clltn.jpg) | ![edge_example_05](./dataset/examples/preprocessor/edges/mcocr_public_145013clltn_canny.jpg) | ![output_example_05](./dataset/examples/preprocessor/imgs/mcocr_public_145013clltn.jpg) |
|![example_06](./dataset/examples/input/mcocr_public_145014ckynq.jpg) | ![edge_example_06](./dataset/examples/preprocessor/edges/mcocr_public_145014ckynq_canny.jpg) | ![output_example_06](./dataset/examples/preprocessor/imgs/mcocr_public_145014ckynq.jpg) |

### Text Detection

- Folder ./src/text_detector contains code for detect texts of the document in a image using [Differentiable Binarization (DB)](https://arxiv.org/pdf/1911.08947.pdf). It can not only detect texts in the document very well but also with real-time inference speed. 

- DBNet code is taken from [PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR). I used DBNet model with [this config](https://github.com/PaddlePaddle/PaddleOCR/blob/release/2.4/configs/det/ch_ppocr_v2.0/ch_det_res18_db_v2.0.yml) and [trained weights](https://paddleocr.bj.bcebos.com/dygraph_v2.0/ch/ch_ppocr_server_v2.0_det_infer.tar).

- Example Image:

| Output Image       |
| :---------------:|
|![example_01](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145013acjke.jpg)|
|![example_02](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145013pedoz.jpg)|
|![example_03](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145014ehtue.jpg)|
|![example_04](./dataset/examples/text_detector/visualize_imgs/mcocr_val_145114rqxas.jpg)|
|![example_05](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145013clltn.jpg)|
|![example_06](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145014ckynq.jpg)|

### Rotation Correction

- Folder ./src/rotation_corrector contains code to train rotation angle classification model (0 or 180) and rotate straight back for documents that can be tilted and rotated 180 degree upside down. 

- First, the documents will be rotated with angle that calculated based on the ratio of y coordinate to x coordinate of the longer edge vector of the bounding box to determine the inclination of the text. So after rotating, the texts on the document will be horizontal.

- Second, even though the texts is horizontal, but the texts can still be rotated 180 degree upside down. So, we need to rotate straight back for documents that are rotated 180 degree upside down. I trained a rotation angle classification model to predict if the document is lying upside down.

- File ./src/rotation_corrector/prepare_data.py contains code to prepare data to train rotation angle classification model using confidence score of trained VietOCR models to classify the angle to be rotated (0 or 180). Before running this script, we need to do preprocessing and text detection on mcocr_train_data/train_images:

```
python3 prepare_data.py
```

- File ./src/rotation_corrector/train_classifier.py contains code to train and evaluate rotation angle classification model with the data created above. Training script to train rotation angle classification model:

```
python3 train_classifer.py --img_dir "../../dataset/data0_or_180" --model_name "tf_efficientnet_b0_ns" --path_csv "../../dataset/data0_or_180.csv" --train_bs 64 --val_bs 128 --num_workers 4 --lr 1e-3 --num_epochs 20 --num_epochs_per_train 20 --min_lr 1e-5 --accum_grad 1 --save_dir {save_dir} --save_iter 10
```

- File ./src/rotation_corrector/search_thresh.py contains code to search best classification thresh of rotation angle classification model on validation data. Script:

```
python3 search_thresh.py --img_dir "../../dataset/data0_or_180" --model_name "tf_efficientnet_b0_ns" --path_csv "../../dataset/data0_or_180.csv" --resume {path_to_trained_weight} --val_bs 128 --num_workers 4
```

- Example Image:

| Input Image       | Output Image|
| :---------------:| :---------------:|
|![example_01](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145013acjke.jpg)| ![example_01](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_public_145013acjke.jpg) |
|![example_02](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145013pedoz.jpg)| ![example_02](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_public_145013pedoz.jpg) |
|![example_03](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145014ehtue.jpg)| ![example_03](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_public_145014ehtue.jpg) |
|![example_04](./dataset/examples/text_detector/visualize_imgs/mcocr_val_145114rqxas.jpg)| ![example_04](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_val_145114rqxas.jpg) |
|![example_05](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145013clltn.jpg)| ![example_05](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_public_145013clltn.jpg) | 
|![example_06](./dataset/examples/text_detector/visualize_imgs/mcocr_public_145014ckynq.jpg)| ![example_06](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_public_145014ckynq.jpg) |

### Text Recognition

- Folder ./src/text_recognizer contains code to prepare data, train and recognize text on the documents using [VietOCR](https://github.com/pbcquoc/vietocr). [VietOCR](https://github.com/pbcquoc/vietocr) is a framework for Vietnamese Text Recognition task and its models have good generalization, high accuracy on a new dataset.

- File ./src/text_recognizer/prepare_data.py contains code to prepare data to train text recognition model on MC-OCR dataset by cropping the text's image in the input image based on the ground truth bounding boxes provided in MC-OCR dataset. Prepare data script for text recognition task:

```
python3 prepare_data.py
```

- File ./src/text_recognizer/train.py contains code to finetune vgg_seq2seq VietOCR model on data created above. Finetune script:

```
python3 train.py 
```

- Performance of vgg_seq2seq VietOCR model before and after finetuning on MC-OCR dataset:

| Model           | acc_full_seq       | acc_per_char | 
| :---------------| :----------------| :---------| 
| Before Finetuning | 0.4464           | 0.7950    |
| After Finetuning | 0.7277             | 0.8929    | 

- Example Image:

| Output Image       |
| :---------------:|
|![example_01](./dataset/examples/text_recognizer/visualize_imgs/mcocr_public_145013acjke.jpg)|
|![example_02](./dataset/examples/text_recognizer/visualize_imgs/mcocr_public_145013pedoz.jpg)|
|![example_03](./dataset/examples/text_recognizer/visualize_imgs/mcocr_public_145014ehtue.jpg)|
|![example_04](./dataset/examples/text_recognizer/visualize_imgs/mcocr_val_145114rqxas.jpg)|
|![example_05](./dataset/examples/text_recognizer/visualize_imgs/mcocr_public_145013clltn.jpg)|
|![example_06](./dataset/examples/text_recognizer/visualize_imgs/mcocr_public_145014ckynq.jpg)|

### Key Information Extraction

- Folder ./src/key_information_extractor contains code to prepare data, train and extract valuable texts from documents using [PICK](https://arxiv.org/pdf/2004.07464.pdf). PICK uses a lot of features including text, image and position features to obtain a richer semantic representation, so it is a very strong and good model in Key Information Extraction task with a variety of structures in the documents. PICK code is taken from [PICK-pytorch](https://github.com/wenwenyu/PICK-pytorch).

- File ./src/key_information_extractor/prepare_data.py contains code to prepare data to train PICK model by using some rules to fix misannotation, missing annotation on MC-OCR dataset. Before running this script, we need to do preprocessing, text detection, rotation correction and text recognition on mcocr_train_data/train_images:

```
python3 prepare_data.py
```

- Training script to train PICK model from scratch on fixed MC-OCR dataset:

```
cd ./PICK-pytorch
CUDA_VISIBLE_DEVICES=0 python3 -m torch.distributed.launch --nnode=1 --node_rank=0 --nproc_per_node=1 --master_addr=127.0.0.1 --master_port=5555 train.py -c "./config.json" -d 0 --local_world_size 1
cd ../
```

- Performance of PICK model on validation dataset of fixed MC-OCR:

| Model           | mEF | 
| :---------------| :----------------|
| PICK |    0.9578        |

- Example Image (the green bbox: SELLER, the blue bbox: ADDRESS, the pink bbox: TIMESTAMP, the cyan bbox: TOTAL_COST):

| Output Image       |
| :---------------:|
|![example_01](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_public_145013acjke.jpg)|
|![example_02](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_public_145013pedoz.jpg)|
|![example_03](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_public_145014ehtue.jpg)|
|![example_04](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_val_145114rqxas.jpg)|
|![example_05](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_public_145013clltn.jpg)|
|![example_06](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_public_145014ckynq.jpg)|

### Pretrained Weights

- Pretrained weights after download should be placed in the corresponding folder in each row. If not, please modify path weight of pretrained weights of each corresponding config file in folder ./src/configs.

| Model         | weights | corresponding config  |  corresponding folder |
| :---------------| :----------------| :----------------| :----------------|
| DBNet| [link](https://drive.google.com/drive/folders/1k0_dnx5NiTUEdMnBDPX6XDamtwQ4H8EJ?usp=sharing) | text_detector.yaml | src/weights/text_detector |
| Efficientnet-B0 | [link](https://drive.google.com/file/d/1OqFfohYlZgP3CuNIzg1AnlYhPpYaLT70/view?usp=sharing) | rotation_corrector.yaml | src/weights/rotation_corrector |
| VietOCR (vgg-seq2seq) | [link](https://drive.google.com/file/d/1cXoC9ty9Wk11Ne1HhkUgnhdHFlRd6CTS/view?usp=sharing) | text_recognizer.yaml | src/weights/text_recognizer |
| PICK | [link](https://drive.google.com/file/d/1K78NX09ghG4PUf6DeX8bSCmVWt2ma5N6/view?usp=sharing) |key_information_extractor.yaml | src/weights/key_information_extractor |
### Inference

File ./src/main.py contains inference script on image directory:

- img_dir argument to specify the path to the image directory, gpu string argument to specify device id of gpu (ex: "0" for gpu device id 0) or not specify to use cpu, use_kie argument to specify whether run key_information_extractor module or not (currently KIE module only supports for receipt documents).

```
bash reset.sh
python3 main.py --img_dir "../dataset/mc_ocr_test" --gpu "0" --use_kie
```

- Outputs:
    + Folder ./dataset/preprocessor contains output of Preprocessing stage. It contains two subfolders: imgs (preprocessed images) and txt (txt file contains four points of the largest contour). 
    + Folder ./dataset/text_detector contains output of Text Detection stage. It contains two subfolders: txt (txt file contains the detected bounding boxes of texts) and visualize_imgs (preprocessed images + visualize bounding boxes).
    + Folder ./dataset/rotation_corrector contains output of Rotation Correction stage. It contains three subfolders: imgs (rotated images) and txt (rotated bounding boxes) and visualize_imgs (rotated images + visualize rotated bounding boxes).
    + Folder ./dataset/text_recognizer contains output of Text Recognition stage. It contains two subfolders: txt (rotated bounding boxes + transcripts) and visualize_imgs (rotated images + visualize rotated bounding boxes + visualize transcripts).
    + Folder ./dataset/key_information_extractor contains output of Key Information Extraction stage. It contains three subfolders: boxes_and_transcripts (tsv file has the same content as txt file of ./dataset/text_recognizer/txt) and txt (txt file contains extracted rotated bounding boxes + transcripts by KIE) and visualize_imgs (rotated_images + visualize extracted rotated bounding boxes + visualize extracted transcripts).

### Results

- Input:

| Input Image       | Document Type |
| :---------------:| :---------------:|
|![example_01](./dataset/examples/input/mcocr_val_145115xkwvm.jpg)| Receipt |
|![example_02](./dataset/examples/input/personal_id.jpg)| Personal ID |
|![example_03](./dataset/examples/input/page_book.jpg)| Book Page |
|![example_04](./dataset/examples/input/administrative_documents.jpg)| Administrative Documents |
|![example_05](./dataset/examples/input/degree.jpg)| Degree |

- Preprocessing:

| Output Image       |
| :---------------:|
| ![example_01](./dataset/examples/preprocessor/imgs/mcocr_val_145115xkwvm.jpg) |
| ![example_02](./dataset/examples/preprocessor/imgs/personal_id.jpg) |
| ![example_03](./dataset/examples/preprocessor/imgs/page_book.jpg) |
| ![example_04](./dataset/examples/preprocessor/imgs/administrative_documents.jpg) |
| ![example_05](./dataset/examples/preprocessor/imgs/degree.jpg) |

- Text Detection:

| Output Image       |
| :---------------:|
| ![example_01](./dataset/examples/text_detector/visualize_imgs/mcocr_val_145115xkwvm.jpg) |
| ![example_02](./dataset/examples/text_detector/visualize_imgs/personal_id.jpg) |
| ![example_03](./dataset/examples/text_detector/visualize_imgs/page_book.jpg) |
| ![example_04](./dataset/examples/text_detector/visualize_imgs/administrative_documents.jpg) |
| ![example_05](./dataset/examples/text_detector/visualize_imgs/degree.jpg) |

- Rotation Correction:

| Output Image       |
| :---------------:|
| ![example_01](./dataset/examples/rotation_corrector/visualize_imgs/mcocr_val_145115xkwvm.jpg) |
| ![example_02](./dataset/examples/rotation_corrector/visualize_imgs/personal_id.jpg) |
| ![example_03](./dataset/examples/rotation_corrector/visualize_imgs/page_book.jpg) |
| ![example_04](./dataset/examples/rotation_corrector/visualize_imgs/administrative_documents.jpg) |
| ![example_05](./dataset/examples/rotation_corrector/visualize_imgs/degree.jpg) |

- Text Recognition:

| Output Image       |
| :---------------:|
| ![example_01](./dataset/examples/text_recognizer/visualize_imgs/mcocr_val_145115xkwvm.jpg) |
| ![example_02](./dataset/examples/text_recognizer/visualize_imgs/personal_id.jpg) |
| ![example_03](./dataset/examples/text_recognizer/visualize_imgs/page_book.jpg) |
| ![example_04](./dataset/examples/text_recognizer/visualize_imgs/administrative_documents.jpg) |
| ![example_05](./dataset/examples/text_recognizer/visualize_imgs/degree.jpg) |

- Key Information Extraction:

| Output Image       |
| :---------------:|
| ![example_01](./dataset/examples/key_information_extractor/visualize_imgs/mcocr_val_145115xkwvm.jpg) |


### Notebooks
Folder ./src/notebooks contains notebooks for EDA:

- [Exploratory Data Analysis Notebook](./src/notebooks/MCOCR_EDA.ipynb)
### Awesome Resources

[Pytorch](https://github.com/pytorch/pytorch)✨\
[PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR)✨\
[VietOCR](https://github.com/pbcquoc/vietocr)✨\
[PICK-pytorch](https://github.com/wenwenyu/PICK-pytorch)✨


